import pymel as pm
import maya.cmds as cmds
from collections import OrderedDict
import zBuilder.builders.ziva as zva
import zBuilder.zMaya as mz
import maya.mel as mm
import zivaTools as zTool;reload(zTool)
import omUtil as omu;reload(omu)

def replaceMuscle(muscleDirPath):
    if cmds.ls(sl=True):
        # Filter only zTissues
        tissueList = []
        [tissueList.append(tissue) for tissue in cmds.ls(sl=True) if mm.eval('zQuery -t "zTissue" {0}'.format(tissue))]

    if tissueList:
        # When muscles are attached to each other. At least one muscle needs to remain in the scene.
        if len(cmds.ls(type = "zTissue")) > 1: # If more then one tissue in scene. Fat pass only has one tissue, and therfore can be updated independently. 
            if len(tissueList) == len(cmds.ls(type = "zTissue")):
                raise IndexError('Do not use this tool to update all muscles in the scene. Ziva requires at least one muscle to be present during the execution of this function. Instead save out a *.ziva file and rebuild')

        # Delete materials in obj directory before Maya can even attempt to list them
        [cmds.sysFile(muscleDirPath + "\\" + mat, delete=True) for mat in cmds.getFileList(folder=muscleDirPath, filespec='*.mtl')]

        # Verify Obj's exist in muscleDirPath
        if not cmds.getFileList(folder=muscleDirPath, filespec='*.obj'):
            raise IndexError('No Objects (*.obj) found in specified directory')
        else:
            # filesList is used when importing obj's
            filesList = cmds.getFileList(folder=muscleDirPath, filespec='*.obj')
            # converts list to lowercase for comparison. & remove suffix (.obj)
            filesListLowercase = [file.lower()[:-4] for file in cmds.getFileList(folder=muscleDirPath, filespec='*.obj')]
            # Store tissue : tissue obj to dict if they match
            filesListDict = {}
            [filesListDict.update({tissue : filesListLowercase.index(tissue.lower())}) for tissue in tissueList if tissue.lower() in filesListLowercase]

        # Store muscle : muscle parent
        if filesListDict:
            tissueParentDict={}
            [tissueParentDict.update({key : cmds.listRelatives(key, p=True)[0]}) for key, value in filesListDict.items() if cmds.listRelatives(key, p=True)]

        # Store custom loa setup   
        customLOANodes = []
        for key, value in filesListDict.items():
            fiberList = mm.eval('zQuery -t "zFiber" {0}'.format(key)) # Check for existing fiber:
            if fiberList:
                for fiber in fiberList:
                    if cmds.listConnections(fiber+'.muscleGrowthScale'): # This port will be connected if using custom loa setup
                        customNodes = cmds.listHistory(cmds.listConnections(fiber+'.muscleGrowthScale'), f=0, levels=3)
                        for node in customNodes:
                            if '_LOACustomCurveInfo' in node:
                                customLOANodes.append(node)

        customLOADict = OrderedDict()
        if customLOANodes:
            for node in customLOANodes:
                if cmds.listConnections(node+'.inputCurve'): # Make sure curveInfo node is being used
                    customLOASettings = OrderedDict()
                    customLOASettings[node+'.growthScale'] = [cmds.getAttr(node+'.growthScale'), cmds.getAttr(node+'.growthScale', type=True)]
                    customLOASettings[node+'.loaFlexionChk'] = [cmds.getAttr(node+'.loaFlexionChk'), cmds.getAttr(node+'.loaFlexionChk', type=True)]
                    customLOASettings[node+'.loaExtensionChk'] = [cmds.getAttr(node+'.loaExtensionChk'), cmds.getAttr(node+'.loaExtensionChk', type=True)]
                    customLOASettings[node+'.flexionValue'] = [cmds.getAttr(node+'.flexionValue'), cmds.getAttr(node+'.flexionValue', type=True)]
                    customLOASettings[node+'.extensionValue'] = [cmds.getAttr(node+'.extensionValue'), cmds.getAttr(node+'.extensionValue', type=True)]

                    customLOADict['loaNodeBkup', cmds.getAttr(node+'.inputCrv'), cmds.getAttr(node+'.musFiber')] = customLOASettings

        # Get LOA curves if child of tissue
        loaCurveList = []
        for key, value in filesListDict.items():
            # Check for nurbs curve with '_LOA_Curve' in the name
            [loaCurveList.append(loaCurve) for loaCurve in cmds.listRelatives(key, c=True) if ('_LOA_Curve' in loaCurve) and (cmds.objectType(omu.getDagPath(loaCurve, shape=True))=='nurbsCurve')]

        # Select filtered list of tissues and store ziva setup
        cmds.select(None)
        for key, value in filesListDict.items():
            cmds.select(key, add=True)
        z = zva.Ziva()
        z.retrieve_from_scene_selection()
        mm.eval("ziva -rm")

        # Un-parent any LOA curves if they exist
        if loaCurveList:
            zTool.loaUnParent(writeOut=False, loaCurves=loaCurveList)
        
        # Delete tissues
        for key, value in filesListDict.items():
            cmds.delete(key)

        # Import tissue obj's and rename them.
        for muscle, index in filesListDict.items():
            importFile = cmds.file(str(muscleDirPath) + '\\' + filesList[index] , i = True, returnNewNodes = True)
            cmds.rename(importFile[0], muscle)

        # Parent new tissue obj's
        for muscle, parent in tissueParentDict.items():
            cmds.parent(muscle, parent)

        # Reparent LOA curves
        if loaCurveList:
            zTool.loaReParent(readIn=False)

        # Build
        cmds.select(None)
        for key, value in filesListDict.items():
            cmds.select(key, add=True)
        z.build()

        # Connect custom LOA setup
        if customLOADict:
            buildLOA = []
            for key , value in customLOADict.iteritems():
                # if key[0] == 'loaNodeBkup':
                inputCrv = key[1] # curve
                zFiber = key[2] # fiber
                muscle = key[2][:-7]
                growthScale = value.items()[0][1] # growthScale
                loaFlexionChk = value.items()[1][1] # loaFlexionChk
                loaExtensionChk = value.items()[2][1] # loaExtensionChk
                flexionValue = value.items()[3][1] # flexionValue
                extensionValue = value.items()[4][1] # extensionValue
                buildLOA.append([flexionValue[0], extensionValue[0], growthScale[0], loaFlexionChk[0], loaExtensionChk[0], inputCrv, zFiber, muscle])
            else:
                pass

            if buildLOA:
                for build in buildLOA:
                    zTool.loaMuscleCustom(*build) # '*' will unpack list

def updateGeo(geoPath):
    filesListDict = {}
    objectBuildList = []
    missingObjectList = []

    if cmds.ls(sl=True):
        meshList = []
        [meshList.append(i) for i in cmds.ls(sl=True) if cmds.objectType(omu.getDagPath(i, shape=True))=='mesh']

    if meshList:
        # Delete materials in obj directory before Maya can even attempt to list them
        [cmds.sysFile(geoPath + "\\" + mat, delete=True) for mat in cmds.getFileList(folder=geoPath, filespec='*.mtl')]

        # Verify Obj's exist in geoPath
        if not cmds.getFileList(folder=geoPath, filespec='*.obj'):
            raise IndexError('No Objects (*.obj) found in specified directory')
        else:
            # filesList is used when importing obj's
            filesList = cmds.getFileList(folder=geoPath, filespec='*.obj')
            # converts list to lowercase for comparison. & remove suffix (.obj)
            filesListLowercase = [file.lower()[:-4] for file in cmds.getFileList(folder=geoPath, filespec='*.obj')]
            # Store item : item obj to dict if they match
            filesListDict = {}
            [filesListDict.update({mesh : filesListLowercase.index(mesh.lower())}) for mesh in meshList if mesh.lower() in filesListLowercase]

        if filesListDict:
            # Import tissue obj's and rename them.
            for mesh, index in filesListDict.items():
                # Freeze modeling
                cmds.bakePartialHistory(mesh, prePostDeformers=True)
                # oldObj = cmds.rename(old+"_UpdateObjectNewShape")
                importFile = cmds.file(str(geoPath) + '\\' + filesList[index] , i = True, returnNewNodes = True)
                importRename = cmds.rename(importFile[0], importFile[0]+"_UpdateObjectNewShape")
                zTool.matchPointPosition(meshList=[importRename, mesh])




        # for obj in objectBuildList:
        #     # Freeze modeling
        #     cmds.bakePartialHistory(obj, prePostDeformers=True)
        #     # Import new file
        #     importObjFile = cmds.file(str(geoPath) + '\\' + obj +".obj", i = True, returnNewNodes = True)
        #     cmds.select(importObjFile[0])
        #     cmds.rename(obj+"_UpdateObjectNewShape")
        #     cmds.select(obj, add=True)
        #     zTool.matchPointPosition()
